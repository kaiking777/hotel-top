package com.hotel.hoteltop.qdRateplan.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.hoteltop.qdRateplan.model.RateplanRecordModel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface RateplanRecordMapper extends BaseMapper<RateplanRecordModel> {
}
