package com.hotel.hoteltop.qdRateplan.controller;

import com.hotel.hoteltop.qdRateplan.service.RateplanService;
import com.hotel.hoteltop.util.RpcResultDTO;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping(value = "/rateplanManager")
public class RateplanController {

    @Resource
    private RateplanService rateplanService;

    @PostMapping(value = "/saveRateplan")
    RpcResultDTO savePrice(@RequestBody Map<String, String> model) {
        Assert.notNull(model, "未获取到锦江酒店id,房型id，产品id(或者价格id)");
        return rateplanService.saveRateplan(model);
    }

    @PostMapping(value = "/delRateplan")
    RpcResultDTO delPrice(@RequestBody Map<String, String> model) {
        Assert.notNull(model, "未获取到锦江酒店id,房型id，产品id(或者价格id)");
        return rateplanService.delRateplan(model);
    }

    @PostMapping(value = "/modifyRateplan")
    RpcResultDTO modifyPrice(@RequestBody Map<String, String> model) {
        Assert.notNull(model, "未获取到锦江酒店id,房型id，产品id(或者价格id)");
        return rateplanService.modifyRateplan(model);
    }

    @PostMapping(value = "/queryRateplan")
    RpcResultDTO queryRateplan(@RequestBody Map<String, String> model) {
        Assert.notNull(model, "未获取到锦江酒店id,房型id，产品id(或者价格id)");
        return rateplanService.queryRateplan(model);
    }


}
