package com.hotel.hoteltop.qdRateplan.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 飞猪端产品信息记录表
 */
@Data
@TableName("HOTEL_RATEPLAN_INFOS_RECORD")
public class RateplanRecordModel implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("hotel_id")
    private String hotelId;

    @TableField("room_type_code")
    private String roomTypeCode;

    @TableField("plan_rate_code")
    private String planRateCode;

    @TableField("top_rpid")
    private Long topRpid;

    @TableField("top_name")
    private String topName;

    @TableField("top_breakfast_count")
    private Integer topBreakfastCount;

    @TableField("top_cancel_policy")
    private String topCancelPolicy;

    @TableField("top_channel")
    private String topChannel;

    @TableField("top_status")
    private String topStatus;


}