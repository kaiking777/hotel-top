package com.hotel.hoteltop.qdRateplan.model;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("HOTEL_PRICES")
public class RateplanModel implements Serializable {

    @TableField("id")
    private String id;

    @TableField("created_at")
    private Date createdAt;

    @TableField("updated_at")
    private Date updatedAt;

    @TableField("deleted_at")
    private Date deletedAt;

    @TableField("HotelNum")
    private String hotelNum;

    @TableField("PlanRateCode")
    private String planRateCode;

    @TableField("RoomTypeCode")
    private String roomTypeCode;

    @TableField("ActivityCode")
    private String activityCode;

    @TableField("TargetDate")
    private String targetDate;

    @TableField("AdultNum")
    private String adultNum;

    @TableField("ChildNum")
    private String childNum;

    @TableField("RoomCount")
    private String roomCount;

    @TableField("EndDate")
    private Long endDate;

    @TableField("Price")
    private Integer price;

    @TableField("Tex")
    private String tex;

    @TableField("OldPrice")
    private Long oldPrice;

    @TableField("Cancellation")
    private String cancellation;

    @TableField("CancelBeforeTime")
    private Date cancelBeforeTime;

    @TableField("CancellationLeftDays")
    private String cancellationLeftDays;

    @TableField("Breakfast")
    private String Breakfast;

    @TableField("RateCodeName")
    private String rateCodeName;

    @TableField("other")
    private String other;
}