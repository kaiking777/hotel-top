package com.hotel.hoteltop.qdRateplan.service;


import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.hotel.hoteltop.constant.ResponseConstant;
import com.hotel.hoteltop.constant.SysParamConstant;
import com.hotel.hoteltop.qdRateplan.mapper.RateplanRecordMapper;
import com.hotel.hoteltop.qdRateplan.mapper.RateplanMapper;
import com.hotel.hoteltop.qdRateplan.model.RateplanRecordModel;
import com.hotel.hoteltop.qdRateplan.model.RateplanModel;
import com.hotel.hoteltop.qdroom.mapper.RoomMapper;
import com.hotel.hoteltop.qdroom.mapper.RoomRecordMapper;
import com.hotel.hoteltop.qdroom.model.RoomModel;
import com.hotel.hoteltop.qdroom.model.RoomRecordModel;
import com.hotel.hoteltop.util.ClientUtil;
import com.hotel.hoteltop.util.RecordOutIdUtil;
import com.hotel.hoteltop.util.ResponseUtils;
import com.hotel.hoteltop.util.RpcResultDTO;
import com.taobao.api.request.XhotelRateplanAddRequest;
import com.taobao.api.request.XhotelRateplanDeleteRequest;
import com.taobao.api.request.XhotelRateplanGetRequest;
import com.taobao.api.request.XhotelRateplanUpdateRequest;
import com.taobao.api.response.XhotelRateplanAddResponse;
import com.taobao.api.response.XhotelRateplanDeleteResponse;
import com.taobao.api.response.XhotelRateplanGetResponse;
import com.taobao.api.response.XhotelRateplanUpdateResponse;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

import static com.hotel.hoteltop.util.ClientUtil.sessionKey;
import static com.hotel.hoteltop.util.TransferUtilFunction.transferPriceModelAddFunc;
import static com.hotel.hoteltop.util.TransferUtilFunction.transferPriceModelUpdateFunc;

@Service
public class RateplanService {

    @Resource
    private RateplanMapper rateplanMapper;

    @Resource
    private RoomMapper roomMapper;

    @Resource
    private RateplanRecordMapper rateplanRecordMapper;

    @Resource
    private RoomRecordMapper roomRecordMapper;


    public void saveRoomCompleteInfo(HashMap<String, String> map) {
        String hotelID = map.get("HotelID");
        ArrayList<RateplanModel> maxTargetDateModelList = queryMaxTargetDateGroupByRoomTypeAndPlanRateCode(hotelID);
        maxTargetDateModelList.forEach(e -> {
            saveRateplanSingle(e);
        });
    }

    public ArrayList<RateplanModel> queryMaxTargetDateGroupByRoomTypeAndPlanRateCode(String hotelID) {
        ArrayList<RateplanModel> maxTargetDateModelList = new ArrayList<>();
        List<RateplanModel> rateplanModels = rateplanMapper.selectList(new QueryWrapper<RateplanModel>().eq("HotelNum", hotelID));
        // 同产品id查询该表会存在多条数据,需要按照roomtype，planRateCode分组，使用最新的一条产品信息
        Map<String, List<RateplanModel>> roomTypeMap = rateplanModels.stream().collect(Collectors.groupingBy(RateplanModel::getRoomTypeCode));
        roomTypeMap.values().forEach(e -> {
            Map<String, List<RateplanModel>> ratePlanRateMap = e.stream().collect(Collectors.groupingBy(RateplanModel::getPlanRateCode));
            ratePlanRateMap.values().forEach(item -> {
                RateplanModel rateplanModel = item.stream().max(Comparator.comparing(RateplanModel::getTargetDate)).get();
                maxTargetDateModelList.add(rateplanModel);
            });
        });
        return maxTargetDateModelList;
    }


    @SneakyThrows
    public RpcResultDTO saveRateplanSingle(RateplanModel rateplanModel) {
        RoomRecordModel roomRecordModel = roomRecordMapper.selectOne(new QueryWrapper<RoomRecordModel>().eq("room_type_code", rateplanModel.getRoomTypeCode()).eq("hotel_id", rateplanModel.getHotelNum()));
        if (roomRecordModel == null) {
            return ResponseUtils.error("未查询到房型信息");
        }

        RoomModel roomModel = roomMapper.selectOne((new QueryWrapper<RoomModel>().eq("HotelID", rateplanModel.getHotelNum()).eq("RoomTypeCode", rateplanModel.getRoomTypeCode())));

        XhotelRateplanAddRequest request = transferPriceModelAddFunc.apply(roomModel, rateplanModel);
        XhotelRateplanAddResponse rsp = ClientUtil.client.execute(request, sessionKey);
        if (!StringUtils.isEmpty(rsp.getErrorCode())) {
            System.out.println("新增产品失败=>" + rsp.getSubMsg() + rsp.getMsg());
            return ResponseUtils.error("新增产品失败=>" + rsp.getSubMsg() + rsp.getMsg());
        }
        // 再查询一次taobao.xhotel.rateplan.get 获取产品信息进行记录
        XhotelRateplanGetRequest req = new XhotelRateplanGetRequest();
        req.setRateplanCode(RecordOutIdUtil.rateplanOuterIdFunction.apply(rateplanModel));
        XhotelRateplanGetResponse ratePlanRsp = ClientUtil.client.execute(req, sessionKey);

        toRatePlanRecord(ratePlanRsp.getBody(),"get", rateplanModel, rsp.getRpid());

        return ResponseUtils.success("新增产品成功");
    }

    @SneakyThrows
    public RpcResultDTO saveRateplan(Map<String, String> model) {
        String roomTypeCode = model.get("RoomTypeCode");
        String hotelID = model.get("HotelNum");
        String planRateCode = model.get("PlanRateCode");

        List<RateplanModel> rateplanModels = rateplanMapper.selectList(new QueryWrapper<RateplanModel>().eq("RoomTypeCode", roomTypeCode)
                .eq("HotelNum", hotelID).eq("PlanRateCode", planRateCode).orderByDesc("TargetDate"));
        if (CollectionUtils.isEmpty(rateplanModels)) {
            return ResponseUtils.error("未查询到该产品信息<HOTEL_PRICES>");
        }
        return saveRateplanSingle(rateplanModels.get(0));
    }

    private void toRatePlanRecord(String bodyStr, String operateStaus, RateplanModel model, Long rpid) {
        Map map = JSONObject.parseObject(bodyStr, Map.class);
        Map response = (Map) map.get("xhotel_rateplan_"+operateStaus+"_response");
        Map rateplan = (Map) response.get("rateplan");
        String name = rateplan.get("name").toString();
        Integer breakfastCount = (Integer) rateplan.get("breakfast_count");
        String cancelPolicy = rateplan.get("cancel_policy").toString();
        String channel = rateplan.get("channel").toString();
        String status = rateplan.get("status").toString();

        RateplanRecordModel recordModel = new RateplanRecordModel();
        recordModel.setHotelId(model.getHotelNum());
        recordModel.setRoomTypeCode(model.getRoomTypeCode());
        recordModel.setPlanRateCode(model.getPlanRateCode());
        recordModel.setTopRpid(rpid);
        recordModel.setTopName(name);
        recordModel.setTopBreakfastCount(breakfastCount);
        recordModel.setTopCancelPolicy(cancelPolicy);
        recordModel.setTopChannel(channel);
        recordModel.setTopStatus(status);

        rateplanRecordMapper.delete(new QueryWrapper<RateplanRecordModel>().eq("room_type_code", model.getRoomTypeCode())
                .eq("hotel_id", model.getHotelNum()).eq("plan_rate_code", model.getPlanRateCode()));
        rateplanRecordMapper.insert(recordModel);
    }


    @SneakyThrows
    public RpcResultDTO modifyRateplan(Map<String, String> model) {
        String roomTypeCode = model.get("RoomTypeCode");
        String hotelID = model.get("HotelNum");
        String planRateCode = model.get("PlanRateCode");
        RoomRecordModel roomRecordModel = roomRecordMapper.selectOne(new QueryWrapper<RoomRecordModel>().eq("room_type_code", roomTypeCode).eq("hotel_id", hotelID));
        if (roomRecordModel == null) {
            return ResponseUtils.error("未查询到房型信息");
        }

        List<RateplanModel> rateplanModels = rateplanMapper.selectList(new QueryWrapper<RateplanModel>().eq("RoomTypeCode", roomTypeCode)
                .eq("HotelNum", hotelID).eq("PlanRateCode", planRateCode).orderByDesc("TargetDate"));
        if (CollectionUtils.isEmpty(rateplanModels)) {
            return ResponseUtils.error("未查询到该产品信息<HOTEL_PRICES>");
        }
        RoomModel roomModel = roomMapper.selectOne((new QueryWrapper<RoomModel>().eq("HotelID", hotelID).eq("RoomTypeCode", roomTypeCode)));

        XhotelRateplanUpdateRequest request = transferPriceModelUpdateFunc.apply(roomModel, rateplanModels.get(0));
        XhotelRateplanUpdateResponse rsp = ClientUtil.client.execute(request, sessionKey);
        if (StringUtils.isEmpty(rsp.getErrorCode())) {
            // api说明:该接口支持修改 也支持添加 所以需要同步更新 产品信息记录表 (说是支持添加 但是好像仅有更新)
//            toRatePlanRecord(rsp.getBody(),"update", rateplanModels.get(0), rsp.getRpid());
            System.out.println(rsp.getMsg());
            return ResponseUtils.success("修改产品成功=>" + rsp.getRpid());
        }

        return ResponseUtils.error("修改产品失败=>" + rsp.getSubMsg());
    }

    /**
     * 删除某酒店下所有产品
     * @param idList
     * @return
     */
    public RpcResultDTO delBatchRateplanForHotel(List<String> idList) {
        List<RateplanRecordModel> recordModelList = rateplanRecordMapper.selectList(new QueryWrapper<RateplanRecordModel>().in("hotel_id", idList));
        doDelRatePlan(recordModelList);
        return ResponseUtils.success();
    }

    public RpcResultDTO delBatchRateplanForHotelForRoomType(List<String> idList) {
        List<RateplanRecordModel> recordModelList = rateplanRecordMapper.selectList(new QueryWrapper<RateplanRecordModel>().in("room_type_code", idList));

        doDelRatePlan(recordModelList);
        return ResponseUtils.success();
    }

    private RpcResultDTO doDelRatePlan(List<RateplanRecordModel> recordModelList){
        recordModelList.forEach(e -> {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("RoomTypeCode", e.getRoomTypeCode());
            hashMap.put("HotelNum", e.getHotelId());
            hashMap.put("PlanRateCode", e.getPlanRateCode());
            delRateplan(hashMap);
        });
        return ResponseUtils.success();
    }

    @SneakyThrows
    public RpcResultDTO delRateplan(Map<String, String> model) {
        String roomTypeCode = model.get("RoomTypeCode");
        String hotelID = model.get("HotelNum");
        String planRateCode = model.get("PlanRateCode");

        RateplanRecordModel rateplanRecordModel = rateplanRecordMapper.selectOne(new QueryWrapper<RateplanRecordModel>().eq("hotel_id", hotelID)
                .eq("plan_rate_code", planRateCode).eq("room_type_code", roomTypeCode));
        if (rateplanRecordModel == null) {
            return ResponseUtils.error("未查询到该房型下存在产品信息");
        }
        XhotelRateplanDeleteRequest req = new XhotelRateplanDeleteRequest();

        req.setRpId(rateplanRecordModel.getTopRpid());
        req.setRateplanCode(SysParamConstant.HZ + hotelID + SysParamConstant.UNDERLINE + roomTypeCode + SysParamConstant.UNDERLINE + planRateCode);

        XhotelRateplanDeleteResponse rsp = ClientUtil.client.execute(req, sessionKey);
        if (rsp.isSuccess()) {
            rateplanRecordMapper.delete(new QueryWrapper<RateplanRecordModel>()
                    .eq("hotel_id", hotelID)
                    .eq("plan_rate_code", planRateCode)
                    .eq("room_type_code", roomTypeCode));
            return ResponseUtils.success("删除产品成功=>" + rateplanRecordModel.getTopRpid());
        }
        return ResponseUtils.error("删除产品失败=>" + rsp.getSubMsg());
    }


    @SneakyThrows
    public RpcResultDTO queryRateplan(Map<String, String> model) {
        String roomTypeCode = model.get("RoomTypeCode");
        String hotelID = model.get("HotelNum");
        String planRateCode = model.get("PlanRateCode");

        RateplanRecordModel rateplanRecordModel = rateplanRecordMapper.selectOne(new QueryWrapper<RateplanRecordModel>().eq("hotel_id", hotelID)
                .eq("plan_rate_code", planRateCode).eq("room_type_code", roomTypeCode));
        if (rateplanRecordModel == null) {
            return ResponseUtils.error("未查询到该房型下存在产品信息");
        }

        XhotelRateplanGetRequest req = new XhotelRateplanGetRequest();
        req.setRpid(rateplanRecordModel.getTopRpid());
        req.setRateplanCode(SysParamConstant.HZ + hotelID + SysParamConstant.UNDERLINE + roomTypeCode + SysParamConstant.UNDERLINE + planRateCode);
        XhotelRateplanGetResponse rsp = ClientUtil.client.execute(req, sessionKey);
        return ResponseUtils.success(rsp.getBody());
    }


}
