package com.hotel.hoteltop.constant;

import lombok.Getter;

@Getter
public enum HotelStatusEnum {

    normal("0", "正常"),

    deleted("-1", "删除"),

    stopsale("-2", "停售"),
    ;

    String code;
    String desc;

    HotelStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}