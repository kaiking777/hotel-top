package com.hotel.hoteltop.constant;

public class SysParamConstant {

    public static String HZ = "HZ_";

    public static String UNDERLINE = "_";

    public static String ZERO = "0";

    public static String ONE = "1";

    public static String empty = "";

    public static String comma = ",";

    public static String trueStr = "true";

    public static String falseStr = "false";
}
