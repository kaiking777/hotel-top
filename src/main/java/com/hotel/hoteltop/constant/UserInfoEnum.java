package com.hotel.hoteltop.constant;

import lombok.Getter;

@Getter
public enum UserInfoEnum {

    appKey("31161382", "TOP分配给应用的AppKey"),

    appSecret("44e5e3369173fabc4610d345b90f002e", "api接口密钥"),

    sessionKey("620252259c70e2eeb4c191b43b29a0ZZecd9c536273017f2208925335625", "会话密钥"),
    ;

    String desc;
    String code;

    UserInfoEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}