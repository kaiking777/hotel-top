package com.hotel.hoteltop.qdroom.controller;

import com.hotel.hoteltop.constant.SysParamConstant;
import com.hotel.hoteltop.qdroom.service.RoomService;
import com.hotel.hoteltop.util.RpcResultDTO;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import javax.annotation.Resource;
import java.util.Map;

@RestController
@RequestMapping(value = "/roomManager")
public class RoomController {

    @Resource
    private RoomService roomService;


    @PostMapping(value = "/saveRoom")
    RpcResultDTO saveRoom(@RequestBody Map<String, String> model) {
        Assert.notNull(model, "未获取到酒店id");
        return roomService.saveRoom(model);
    }

    @PostMapping(value = "/delRoom")
    RpcResultDTO delRoom(@RequestBody Map<String, String> model) {
        Assert.notNull(model, "未获取到酒店id");
        return roomService.delRoom(model);
    }

    @PostMapping(value = "/modifyRoom")
    RpcResultDTO modifyRoom(@RequestBody Map<String, String> model) {
        Assert.notNull(model, "未获取到酒店id");
        return roomService.modifyRoom(model, SysParamConstant.empty);
    }

    @PostMapping(value = "/queryRoom")
    RpcResultDTO queryRoom(@RequestBody Map<String, String> model) {
        Assert.notNull(model, "未获取到酒店id");
        return roomService.queryRoom(model);
    }

}
