package com.hotel.hoteltop.qdroom.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.hotel.hoteltop.constant.SysParamConstant;
import com.hotel.hoteltop.qdRateplan.service.RateplanService;
import com.hotel.hoteltop.qdhotel.mapper.HotelRecordMapper;
import com.hotel.hoteltop.qdhotel.model.HotelRecordModel;
import com.hotel.hoteltop.qdroom.mapper.RoomMapper;
import com.hotel.hoteltop.qdroom.mapper.RoomRecordMapper;
import com.hotel.hoteltop.qdroom.model.RoomModel;
import com.hotel.hoteltop.qdroom.model.RoomRecordModel;
import com.hotel.hoteltop.util.ClientUtil;
import com.hotel.hoteltop.util.ResponseUtils;
import com.hotel.hoteltop.util.RpcResultDTO;
import com.taobao.api.request.*;
import com.taobao.api.response.*;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hotel.hoteltop.util.ClientUtil.sessionKey;
import static com.hotel.hoteltop.util.TransferUtilFunction.transferRoomModelAddFunc;
import static com.hotel.hoteltop.util.TransferUtilFunction.transferRoomModelUpdateFunc;

@Service
public class RoomService {

    @Resource
    private RoomMapper roomMapper;
    @Resource
    private HotelRecordMapper hotelRecordMapper;
    @Resource
    private RoomRecordMapper roomRecordMapper;
    @Resource
    private RateplanService rateplanService;



    @SneakyThrows
    public RpcResultDTO saveRoomSingle(RoomModel roomModel) {
        // 增加查询记录表的hid
        HotelRecordModel recordModel = hotelRecordMapper.selectOne(new QueryWrapper<HotelRecordModel>().eq("hotel_id", roomModel.getHotelID()));
        Assert.notNull(recordModel, "请先添加酒店信息");

        XhotelRoomtypeAddRequest req = transferRoomModelAddFunc.apply(recordModel, roomModel);
        XhotelRoomtypeAddResponse rsp = ClientUtil.client.execute(req, sessionKey);

        if (StringUtils.isNotBlank(rsp.getErrorCode())) {
            System.out.println(rsp.getMsg());
            return ResponseUtils.error(rsp.getSubMsg());
        }
        // 落库飞猪端的房型信息
        toHotelRecord(rsp.getBody(), roomModel.getRoomTypeCode(), roomModel.getHotelID(), "add");

        return ResponseUtils.success("新增房型成功");
    }

    public RpcResultDTO saveRoomCompleteInfo(Map<String, String> model) {
        String hotelID = model.get("HotelID");
        List<RoomModel> roomModelList = roomMapper.selectList(new QueryWrapper<RoomModel>().eq("HotelID", hotelID));
        roomModelList.forEach(e -> {
            saveRoomSingle(e);
        });
        return ResponseUtils.success("新增房型成功");
    }


    public RpcResultDTO saveRoom(Map<String, String> model) {
        String roomTypeCode = model.get("RoomTypeCode");
        String hotelID = model.get("HotelID");

        RoomModel roomModel = roomMapper.selectOne(new QueryWrapper<RoomModel>().eq("HotelID", hotelID).eq("RoomTypeCode", roomTypeCode));
        if (roomModel == null){
            return ResponseUtils.error("未查询到房型信息");
        }
        saveRoomSingle(roomModel);
        return ResponseUtils.success("新增房型成功");
    }

    private void toHotelRecord(String body, String roomTypeCode, String hotelID, String operateStr) {
        Map parseObject = JSONObject.parseObject(body, Map.class);
        Map roomTypeMap = (Map) parseObject.get("xhotel_roomtype_"+operateStr+"_response");
        Map xroomtype = (Map) roomTypeMap.get("xroomtype");
        Long rid = (Long) xroomtype.get("rid");
        Integer status = (Integer) xroomtype.get("status");
        Map s_roomtype = (Map) xroomtype.get("s_roomtype");
        String roomTypeName = xroomtype.get("name").toString();

        Integer srid = (Integer) s_roomtype.get("srid");
        String name = s_roomtype.get("name").toString();


        RoomRecordModel roomRecordModel = new RoomRecordModel();
        roomRecordModel.setHotelId(hotelID);
        roomRecordModel.setRoomTypeCode(roomTypeCode);
        roomRecordModel.setRid(rid);
        roomRecordModel.setStatus(status);
        roomRecordModel.setSRoomtypeName(name);
        roomRecordModel.setSRoomtypeSrid(srid);
        roomRecordModel.setName(roomTypeName);

        roomRecordMapper.delete(new QueryWrapper<RoomRecordModel>().eq("hotel_id", hotelID).eq("room_type_code", roomTypeCode));
        roomRecordMapper.insert(roomRecordModel);
    }


    @SneakyThrows
    public RpcResultDTO delBatchRoom(List<String> idList) {
        List<RoomRecordModel> roomRecordModels = roomRecordMapper.selectList(new QueryWrapper<RoomRecordModel>().in("hotel_id", idList));

        roomRecordModels.forEach(e -> {
            HashMap<String, String> map = new HashMap<>();
            map.put("RoomTypeCode", e.getRoomTypeCode());
            map.put("HotelID", e.getHotelId());
            delRoom(map);
        });
        return ResponseUtils.success("删除房型成功");
    }

    /**
     * 删除房型接口 飞猪端的信息记录表要删除
     * @param model
     * @return
     */
    @SneakyThrows
    public RpcResultDTO delRoom(Map<String, String> model) {
        String roomTypeCode = model.get("RoomTypeCode");
        String hotelID = model.get("HotelID");
        RoomRecordModel roomRecordModel = roomRecordMapper.selectOne(new QueryWrapper<RoomRecordModel>().eq("room_type_code", roomTypeCode).eq("hotel_id", hotelID));
        if (roomRecordModel == null) {
            return ResponseUtils.error("未查询到房型信息");
        }

        XhotelRoomtypeDeletePublicRequest roomReq = new XhotelRoomtypeDeletePublicRequest();
        roomReq.setOuterRid(SysParamConstant.HZ + hotelID + SysParamConstant.UNDERLINE + roomTypeCode);
        roomReq.setOperator("admin");
        roomReq.setRid(roomRecordModel.getRid());

        XhotelRoomtypeDeletePublicResponse execute = ClientUtil.client.execute(roomReq, sessionKey);
        System.out.println(execute.getBody());
        roomRecordMapper.delete(new QueryWrapper<RoomRecordModel>().eq("room_type_code", roomTypeCode).eq("hotel_id", hotelID));
        // 对应的产品也需要删除
        ArrayList<String> list = new ArrayList<>();
        list.add(roomTypeCode);
        rateplanService.delBatchRateplanForHotelForRoomType(list);

        return ResponseUtils.success("删除房型成功,并级联删除了产品信息");
    }

    public RpcResultDTO modifyBatchRoom(Map<String, String> model, String roomStatus) {
        String hotelID = model.get("HotelID");
        List<RoomModel> roomModelList = roomMapper.selectList(new QueryWrapper<RoomModel>().eq("HotelID", hotelID));
        roomModelList.forEach(e -> {
            HashMap<String, String> map = new HashMap<>();
            map.put("RoomTypeCode", e.getRoomTypeCode());
            map.put("HotelID", hotelID);
            modifyRoom(map, roomStatus);
        });
        return ResponseUtils.success("修改房型状态成功");
    }

    @SneakyThrows
    public RpcResultDTO modifyRoom(Map<String, String> model, String roomStatus) {
        String roomTypeCode = model.get("RoomTypeCode");
        String hotelID = model.get("HotelID");

        RoomModel roomModel = roomMapper.selectOne((new QueryWrapper<RoomModel>().eq("HotelID", hotelID).eq("RoomTypeCode", roomTypeCode)));
        Assert.notNull(roomModel, "未获取到房型信息");
        XhotelRoomtypeUpdateRequest updateRequest = transferRoomModelUpdateFunc.apply(roomModel);
        if (StringUtils.isNotBlank(roomStatus)) {
            updateRequest.setStatus(Byte.valueOf(roomStatus));
        }
        XhotelRoomtypeUpdateResponse updateResponse = ClientUtil.client.execute(updateRequest, sessionKey);
        if (StringUtils.isNotBlank(updateResponse.getErrorCode())) {
            System.out.println(updateResponse.getMsg());
            return ResponseUtils.error(updateResponse.getSubMsg());
        }
        // 飞猪api说明 ID不存在自动新增，所以此处需要重新落库
        toHotelRecord(updateResponse.getBody(), roomTypeCode, hotelID, "update");
        return ResponseUtils.success("推送修改信息成功");
    }

    @SneakyThrows
    public RpcResultDTO queryRoom(Map<String, String> model) {
        String roomTypeCode = model.get("RoomTypeCode");
        String hotelID = model.get("HotelID");
        RoomRecordModel roomRecordModel = roomRecordMapper.selectOne(new QueryWrapper<RoomRecordModel>().eq("room_type_code", roomTypeCode).eq("hotel_id", hotelID));
        if (roomRecordModel == null) {
            return ResponseUtils.error("请先添加房型");
        }

        XhotelRoomtypeGetRequest req = new XhotelRoomtypeGetRequest();

        req.setOuterId(SysParamConstant.HZ + hotelID + SysParamConstant.UNDERLINE + roomTypeCode);
        req.setRid(roomRecordModel.getRid());
        XhotelRoomtypeGetResponse rsp = ClientUtil.client.execute(req, sessionKey);
        return ResponseUtils.success(rsp.getBody());
    }
}
