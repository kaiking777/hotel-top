package com.hotel.hoteltop.qdroom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.hoteltop.qdhotel.model.HotelRecordModel;
import com.hotel.hoteltop.qdroom.model.RoomRecordModel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface RoomRecordMapper extends BaseMapper<RoomRecordModel> {
}
