package com.hotel.hoteltop.qdroom.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.hoteltop.qdhotel.model.HotelModel;
import com.hotel.hoteltop.qdroom.model.RoomModel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface RoomMapper extends BaseMapper<RoomModel> {
}
