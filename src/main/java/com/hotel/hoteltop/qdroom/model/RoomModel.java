package com.hotel.hoteltop.qdroom.model;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("HOTEL_ROOM_INFOS")
public class RoomModel implements Serializable {

    @TableField("id")
    private String id;

    @TableField("created_at")
    private Date createdAt;

    @TableField("updated_at")
    private Date updatedAt;

    @TableField("deleted_at")
    private Date deletedAt;

    @TableField("Breakfast")
    private String breakfast;

    @TableField("_breakfast")
    private String _breakfast;

    @TableField("HotelID")
    private String hotelID;

    @TableField("RoomTypeCode")
    private String roomTypeCode;

    @TableField("PlanRateCode")
    private String planRateCode;

    @TableField("MaxOccupancy")
    private Long maxOccupancy;

    @TableField("MaxChildren")
    private String maxChildren;

    @TableField("BedTypeAndOccupancy")
    private String bedTypeAndOccupancy;

    @TableField("RoomName")
    private String roomName;

    @TableField("RoomName_En")
    private String roomNameEn;

    @TableField("Cancellation")
    private String cancellation;

    @TableField("CancellationLeftDays")
    private String cancellationLeftDays;

    @TableField("RoomSize")
    private String roomSize;

    @TableField("ViewBadge")
    private Long viewBadge;

    @TableField("Description")
    private String description;

    @TableField("CancellationPolicies")
    private String cancellationPolicies;

    @TableField("IsSecretPrice")
    private Long IsSecretPrice;

    @TableField("DiscountPolicies")
    private String discountPolicies;

    @TableField("Language")
    private String language;

    @TableField("RoomPicUrl")
    private String roomPicUrl;
}