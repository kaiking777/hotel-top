package com.hotel.hoteltop.qdroom.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
/**
 * 飞猪端房型信息记录表
 */
@Data
@TableName("HOTEL_ROOM_INFOS_RECORD")
public class RoomRecordModel implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    @TableField("rid")
    private Long rid;

    @TableField("status")
    private Integer status;

    @TableField("name")
    private String name;

    @TableField("s_roomtype_srid")
    private Integer sRoomtypeSrid;

    @TableField("s_roomtype_name")
    private String sRoomtypeName;

    @TableField("room_type_code")
    private String roomTypeCode;

    @TableField("hotel_id")
    private String hotelId;

}