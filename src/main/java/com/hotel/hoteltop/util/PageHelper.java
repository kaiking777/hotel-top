package com.hotel.hoteltop.util;

import lombok.Data;

@Data
public class PageHelper {
    private Integer pageNo;
    private Integer pageSize;

}
