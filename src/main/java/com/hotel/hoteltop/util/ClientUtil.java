package com.hotel.hoteltop.util;

import com.hotel.hoteltop.constant.UserInfoEnum;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;

public class ClientUtil {

    public static String serverUrl = "http://gw.api.taobao.com/router/rest";
    public static String appKey = UserInfoEnum.appKey.getCode();
    public static String appSecret = UserInfoEnum.appSecret.getCode();
    public static String sessionKey = UserInfoEnum.sessionKey.getCode();
    public static TaobaoClient client = new DefaultTaobaoClient(serverUrl, appKey, appSecret);

}
