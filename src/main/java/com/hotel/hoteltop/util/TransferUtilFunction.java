package com.hotel.hoteltop.util;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.hotel.hoteltop.constant.SysParamConstant;
import com.hotel.hoteltop.qdRateplan.model.RateplanModel;
import com.hotel.hoteltop.qdhotel.model.HotelModel;
import com.hotel.hoteltop.qdhotel.model.HotelRecordModel;
import com.hotel.hoteltop.qdroom.model.RoomModel;
import com.taobao.api.request.*;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TransferUtilFunction {


    public static Function<HotelModel, XhotelAddRequest> transferHotelModelAddFunc = e -> {
        XhotelAddRequest req = new XhotelAddRequest();
        req.setOuterId(SysParamConstant.HZ + e.getHotelsId());
        // 取小数点后10位
        req.setLatitude(e.getLatitude().substring(e.getLatitude().indexOf(".") + 10));
        req.setName(e.getHotelNameZh());
        req.setNameE(e.getHotelNameEn());
        req.setCity(e.getCityCode());
        req.setProvince(e.getProvinceCode());
        req.setAddress(e.getAddress());
        req.setCountry(e.getCountry());
        if (!"0.0".equals(e.getHotelsStar())) {
            req.setStar(e.getHotelsStar());
        }
        req.setTel(e.getHotelTel());
        System.out.println(JSONObject.toJSONString(req));
        return req;
    };

    public static Function<HotelModel, XhotelUpdateRequest> transferHotelModelModifyFunc = e -> {
        XhotelUpdateRequest req = new XhotelUpdateRequest();
        req.setOuterId(SysParamConstant.HZ + e.getHotelsId());
        // 取小数点后10位
        req.setLatitude(e.getLatitude().substring(e.getLatitude().indexOf(".") + 10));
        req.setName(e.getHotelNameZh());
        req.setNameE(e.getHotelNameEn());
        req.setCity(e.getCityCode());
        req.setProvince(e.getProvinceCode());
        req.setAddress(e.getAddress());
        req.setCountry(e.getCountry());
        if (!"0.0".equals(e.getHotelsStar())) {
            req.setStar(e.getHotelsStar());
        }
        req.setTel(e.getHotelTel());
        System.out.println(JSONObject.toJSONString(req));
        return req;
    };

    static Function<String, List<String>> bedTypeFunc = str -> {
        String replace = str.replace("[", SysParamConstant.empty).replace("]", SysParamConstant.empty);
        List<String> list = Arrays.asList(replace.split(SysParamConstant.comma));
        return list;
    };

    public static BiFunction<HotelRecordModel, RoomModel, XhotelRoomtypeAddRequest> transferRoomModelAddFunc = (recordModel, e) -> {
        XhotelRoomtypeAddRequest req = new XhotelRoomtypeAddRequest();
        req.setOuterId(SysParamConstant.HZ + e.getHotelID() + SysParamConstant.UNDERLINE + e.getRoomTypeCode());
        req.setOutHid(SysParamConstant.HZ + e.getHotelID());
        req.setHid(recordModel.getHid());// 必填
        req.setName(e.getRoomName());

        req.setMaxOccupancy(e.getMaxOccupancy());
        String str = e.getBedTypeAndOccupancy();
        List<String> list = bedTypeFunc.apply(str);

        req.setBedType(list.get(0));
        req.setBedSize(list.get(1));
        req.setArea(e.getRoomSize());

        List<HashMap<String, String>> mapList = new ArrayList<>();
        HashMap<String, String> map = new HashMap<>();
        map.put("url", e.getRoomPicUrl());
        map.put("ismain", SysParamConstant.trueStr);
        mapList.add(map);
        req.setPics(JSONObject.toJSONString(mapList));

        System.out.println(JSONObject.toJSONString(req));
        return req;
    };

    public static Function<RoomModel, XhotelRoomtypeUpdateRequest> transferRoomModelUpdateFunc = e -> {
        XhotelRoomtypeUpdateRequest req = new XhotelRoomtypeUpdateRequest();
        req.setOuterId(SysParamConstant.HZ + e.getHotelID() + SysParamConstant.UNDERLINE + e.getRoomTypeCode());
        req.setName(e.getRoomName());
        req.setVendor("taobao");
        req.setMaxOccupancy(e.getMaxOccupancy());
        String str = e.getBedTypeAndOccupancy();
        List<String> list = bedTypeFunc.apply(str);

        req.setBedType(list.get(0));
        req.setBedSize(list.get(1));
        req.setArea(e.getRoomSize());

        List<HashMap<String, String>> mapList = new ArrayList<>();
        HashMap<String, String> map = new HashMap<>();
        map.put("url", e.getRoomPicUrl());
        map.put("ismain", SysParamConstant.trueStr);
        mapList.add(map);
        req.setPics(JSONObject.toJSONString(mapList));

        System.out.println(JSONObject.toJSONString(req));
        return req;
    };

    static Function<String, Long> breakfastFunc = str -> {
        if ("双早".equals(str)) {
            return 2l;
        }
        if ("单早".equals(str)) {
            return 1l;
        }
        return 0l;
    };

    static Function<String, Map<String, Object>> otherFunc = str -> {
        HashMap<String, Object> otherInfoMap = new HashMap<>();

        if (StringUtils.isBlank(str)) {
            return otherInfoMap;
        }
        otherInfoMap = (HashMap<String, Object>) JSONObject.parseObject(str, Map.class);
        HashMap<String, Object> bookInfo = (HashMap<String, Object>) JSONObject.parseObject(otherInfoMap.get("bookInfo").toString(), Map.class);
        return bookInfo;
    };


    public static BiFunction<RoomModel, RateplanModel, XhotelRateplanAddRequest> transferPriceModelAddFunc = (r, p) -> {
        XhotelRateplanAddRequest req = new XhotelRateplanAddRequest();
        req.setRateplanCode(SysParamConstant.HZ + p.getHotelNum() + SysParamConstant.UNDERLINE + p.getRoomTypeCode() + SysParamConstant.UNDERLINE + p.getPlanRateCode());

        req.setName(r.getRoomName());
        req.setEnglishName(r.getRoomNameEn());
        if (!"会员价".equals(p.getRateCodeName())) {
            int indexOf = p.getRateCodeName().indexOf("折");
            if (p.getRateCodeName().contains("-")) {
                indexOf = p.getRateCodeName().indexOf("-");
            }
            req.setName(p.getRateCodeName().substring(0, indexOf));
        }
        req.setPaymentType(1L);
        req.setBreakfastCount(breakfastFunc.apply(p.getBreakfast()));
        Map<String, Object> otherInfoMap = otherFunc.apply(p.getOther());
        req.setMinDays(Long.valueOf(otherInfoMap.get("minCheckInUnit").toString()));

        Long maxCheckInUnit = Long.valueOf(otherInfoMap.get("maxCheckInUnit").toString());
        req.setMaxDays(maxCheckInUnit > 90l ? 90l : maxCheckInUnit);

        // 需要根据bookUnit进行判断
        String bookUnit = otherInfoMap.get("bookUnit").toString();
        Long minBookUnit = Long.valueOf(otherInfoMap.get("minBookUnit").toString());
        if ("DAY".equals(bookUnit)) {
            req.setMinAdvHours(minBookUnit * 24);
        }
        if ("HOUR".equals(bookUnit)) {
            req.setMinAdvHours(minBookUnit);
        }

        if (p.getCancellation().equals(SysParamConstant.ZERO)) {
            req.setCancelPolicy("{\"cancelPolicyType\":2}");
        }
        if (p.getCancellation().equals(SysParamConstant.ONE)) {
            int hours = p.getCancelBeforeTime().getHours();
            Map<String, Object> map = new HashMap<>();
            map.put("timeBefore", 24 - hours);
            Map<String, Object> map1 = new HashMap<>();
            map1.put("cancelPolicyType", 5);
            map1.put("policyInfo", map);
            req.setCancelPolicy(JSONObject.toJSONString(map1));
        }
        req.setStatus(1l);

        System.out.println(JSONObject.toJSONString(req));
        return req;
    };


    public static BiFunction<RoomModel, RateplanModel, XhotelRateplanUpdateRequest> transferPriceModelUpdateFunc = (r, p) -> {
        XhotelRateplanUpdateRequest req = new XhotelRateplanUpdateRequest();
        req.setRateplanCode(SysParamConstant.HZ + p.getHotelNum() + SysParamConstant.UNDERLINE + p.getRoomTypeCode() + SysParamConstant.UNDERLINE + p.getPlanRateCode());

        req.setName(r.getRoomName());
        req.setEnglishName(r.getRoomNameEn());
        if (!"会员价".equals(p.getRateCodeName())) {
            int indexOf = p.getRateCodeName().indexOf("折");
            if (p.getRateCodeName().contains("-")) {
                indexOf = p.getRateCodeName().indexOf("-");
            }
            req.setName(p.getRateCodeName().substring(0, indexOf));
        }
        req.setPaymentType(1L);
        req.setBreakfastCount(breakfastFunc.apply(p.getBreakfast()));
        Map<String, Object> otherInfoMap = otherFunc.apply(p.getOther());
        req.setMinDays(Long.valueOf(otherInfoMap.get("minCheckInUnit").toString()));

        Long maxCheckInUnit = Long.valueOf(otherInfoMap.get("maxCheckInUnit").toString());
        req.setMaxDays(maxCheckInUnit > 90l ? 90l : maxCheckInUnit);
        // 需要根据bookUnit进行判断
        String bookUnit = otherInfoMap.get("bookUnit").toString();
        Long minBookUnit = Long.valueOf(otherInfoMap.get("minBookUnit").toString());
        if ("DAY".equals(bookUnit)) {
            req.setMinAdvHours(minBookUnit * 24);
        }
        if ("HOUR".equals(bookUnit)) {
            req.setMinAdvHours(minBookUnit);
        }

        if (p.getCancellation().equals(SysParamConstant.ZERO)) {
            req.setCancelPolicy("{\"cancelPolicyType\":2}");
        }
        if (p.getCancellation().equals(SysParamConstant.ONE)) {
            int hours = p.getCancelBeforeTime().getHours();
            Map<String, Object> map = new HashMap<>();
            map.put("timeBefore", 24 - hours);
            Map<String, Object> map1 = new HashMap<>();
            map1.put("cancelPolicyType", 5);
            map1.put("policyInfo", map);
            req.setCancelPolicy(JSONObject.toJSONString(map1));
        }

        System.out.println(JSONObject.toJSONString(req));
        return req;
    };

    public static <T> List<List<T>> partList(List<T> list, int size) {
        if (CollectionUtils.isEmpty(list) || size < 1) {
            return new ArrayList<>();
        }

        List<List<T>> partList = IntStream.range(0, list.size())
                .boxed()
                .collect(Collectors.groupingBy(index -> index / size))
                .values()
                .stream()
                .map(indices -> indices.stream().map(list::get).collect(Collectors.toList()))
                .collect(Collectors.toList());

        return partList;
    }
}
