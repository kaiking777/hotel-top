package com.hotel.hoteltop.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class RpcResultDTO<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer returnCode = 0;
    private String errorCode = "";
    private String errorMessage = "";
    private Map<String, Object> errorProperties = new HashMap();
    private T result;

    public RpcResultDTO() {
    }

    public RpcResultDTO(T result) {
        this.result = result;
    }

    public Integer getReturnCode() {
        return this.returnCode;
    }

    public void setReturnCode(Integer returnCode) {
        this.returnCode = returnCode;
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getResult() {
        return this.result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public Map<String, Object> getErrorProperties() {
        return this.errorProperties;
    }

    public void setErrorProperties(Map<String, Object> errorProperties) {
        this.errorProperties = errorProperties;
    }

    public String toString() {
        return "RpcResultDTO{returnCode=" + this.returnCode + ", errorCode='" + this.errorCode + '\'' + ", errorMessage='" + this.errorMessage + '\'' + ", errorProperties=" + this.errorProperties + ", result=" + this.result + '}';
    }
}