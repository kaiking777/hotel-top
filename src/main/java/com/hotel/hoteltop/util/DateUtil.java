package com.hotel.hoteltop.util;


import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DateUtil {

    public static long calculateTimeDifference(BigDecimal begin, BigDecimal end) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd.HHmmss");

        try {
            Date dateBegin = sdf.parse(begin.toString());
            Date dateEnd = sdf.parse(end.toString());

            long timeBegin = dateBegin.getTime();
            long timeEnd = dateEnd.getTime();

            return Math.abs(timeEnd - timeBegin);
        } catch (ParseException e) {
            throw new RuntimeException("计算时间差失败");
        }
    }


    public static String getNowTime() {
        LocalDateTime dateTime = LocalDateTime.now();
        return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
//        return BigDecimal.valueOf(Double.valueOf(dateStr));
    }


}
