package com.hotel.hoteltop.util;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.hotel.hoteltop.constant.SysParamConstant;
import com.hotel.hoteltop.qdRateplan.model.RateplanModel;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class RecordOutIdUtil {

    public static Function<RateplanModel, String> rateplanOuterIdFunction = model -> {
        if (model == null) {
            return SysParamConstant.empty;
        }
        return SysParamConstant.HZ + model.getHotelNum() + SysParamConstant.UNDERLINE + model.getRoomTypeCode() + SysParamConstant.UNDERLINE + model.getPlanRateCode();
    };

}
