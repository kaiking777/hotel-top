package com.hotel.hoteltop.util;


import com.hotel.hoteltop.constant.ResponseConstant;

public class ResponseUtils {

    public static RpcResultDTO error(Integer returnCode, String errorMessage){
        RpcResultDTO rpcResultDTO = new RpcResultDTO<>();
        rpcResultDTO.setReturnCode(returnCode);
        rpcResultDTO.setErrorMessage(errorMessage);
        return rpcResultDTO;
    }

    public static RpcResultDTO error(String errorMessage){
        return error(ResponseConstant.ERROR, errorMessage);
    }

    public static <T> RpcResultDTO<T> success(T result){
        RpcResultDTO<T> rpcResultDTO = new RpcResultDTO<>();
        rpcResultDTO.setResult(result);
        return rpcResultDTO;
    }

    public static <T> RpcResultDTO<T> success(){
        return new RpcResultDTO<>();
    }
}
