package com.hotel.hoteltop;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.hotel.hoteltop")
public class HotelTopApplication {

    public static void main(String[] args) {
        SpringApplication.run(HotelTopApplication.class, args);
    }

}
