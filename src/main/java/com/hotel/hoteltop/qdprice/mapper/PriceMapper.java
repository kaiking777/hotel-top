package com.hotel.hoteltop.qdprice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.hoteltop.qdRateplan.model.RateplanModel;
import com.hotel.hoteltop.qdroom.model.RoomModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface PriceMapper extends BaseMapper<RateplanModel> {


    // 1 酒店id配置单酒店价格规则
    @Select("SELECT rdi.* from price_rule_detail_info rdi " +
            "   left JOIN price_rule_info pri on pri.id = rdi.price_id" +
            "   where hotel_id =  #{hotelNum} and end_day >  #{targetDate} and start_day <=  #{targetDate} " +
            "   and min_price <=  #{price} and max_price >  #{price} and rdi.levels =  #{planRateCode} ORDER BY rdi.create_time desc LIMIT 1")
    Map<String, String> queryPricesRuleForOne(@Param("hotelNum")String hotelNum, @Param("targetDate")String targetDate,
                                                    @Param("price")Integer price, @Param("planRateCode")String planRateCode);


    // 2 酒店id配置单酒店价格规则
    @Select("SELECT rdi.* from price_rule_detail_info rdi " +
            "   left JOIN price_rule_info pri on pri.id = rdi.price_id" +
            "   where hotel_id =  #{hotelNum} and end_day >  7 and start_day <=  7 " +
            "   and min_price <=  #{price} and max_price >  #{price} and rdi.levels =  #{planRateCode} ORDER BY rdi.create_time desc LIMIT 1")
    Map<String, String> queryPricesRuleForTwo(@Param("hotelNum")String hotelNum, @Param("targetDate")String targetDate,
                                                    @Param("price")Integer price, @Param("planRateCode")String planRateCode);


    // 3 酒店id未配置单酒店价格规则
    @Select("SELECT rdi.* FROM price_rule_detail_info rdi " +
            "   LEFT JOIN price_rule_info pri ON pri.id = rdi.price_id " +
            "   LEFT JOIN hotel_group_info gi on gi.group_id = pri.group_id" +
            "   LEFT JOIN hotel_group_detail_info gdi on gdi.group_id = gi.group_id" +
            "   where gdi.hotel_id = #{hotelNum} and end_day > #{targetDate} " +
            "   AND start_day <= #{targetDate} " +
            "   AND min_price <= #{price} AND max_price > #{price} " +
            "   AND rdi.levels = #{planRateCode} ORDER BY rdi.create_time desc LIMIT 1")
    Map<String, String> queryPricesRuleForThree(@Param("hotelNum")String hotelNum, @Param("targetDate")String targetDate,
                                                      @Param("price")Integer price, @Param("planRateCode")String planRateCode);

    // 4 酒店id未配置单酒店价格规则
    @Select("SELECT rdi.* FROM price_rule_detail_info rdi " +
            "   LEFT JOIN price_rule_info pri ON pri.id = rdi.price_id " +
            "   LEFT JOIN hotel_group_info gi on gi.group_id = pri.group_id" +
            "   LEFT JOIN hotel_group_detail_info gdi on gdi.group_id = gi.group_id" +
            "   where gdi.hotel_id = #{hotelNum} and end_day > 7 " +
            "   AND start_day <= 7 " +
            "   AND min_price <= #{price} AND max_price > #{price} " +
            "   AND rdi.levels = #{planRateCode} ORDER BY rdi.create_time desc LIMIT 1")
    Map<String, String> queryPricesRuleForFour(@Param("hotelNum")String hotelNum, @Param("targetDate")String targetDate,
                                                     @Param("price")Integer price, @Param("planRateCode")String planRateCode);


}
