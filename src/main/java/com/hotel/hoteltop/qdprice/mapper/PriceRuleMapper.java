package com.hotel.hoteltop.qdprice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.hoteltop.qdRateplan.model.RateplanModel;
import com.hotel.hoteltop.qdprice.model.PriceRuleModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
@Mapper
public interface PriceRuleMapper extends BaseMapper<PriceRuleModel> {



}
