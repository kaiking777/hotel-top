package com.hotel.hoteltop.qdprice.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.hotel.hoteltop.constant.SysParamConstant;
import com.hotel.hoteltop.qdRateplan.mapper.RateplanMapper;
import com.hotel.hoteltop.qdRateplan.mapper.RateplanRecordMapper;
import com.hotel.hoteltop.qdRateplan.model.RateplanModel;
import com.hotel.hoteltop.qdRateplan.model.RateplanRecordModel;
import com.hotel.hoteltop.qdRateplan.service.RateplanService;
import com.hotel.hoteltop.qdprice.mapper.PriceMapper;
import com.hotel.hoteltop.qdprice.mapper.PriceRuleDetailMapper;
import com.hotel.hoteltop.qdprice.mapper.PriceRuleMapper;
import com.hotel.hoteltop.qdprice.model.PriceRuleDetailModel;
import com.hotel.hoteltop.qdprice.model.PriceRuleModel;
import com.hotel.hoteltop.util.ClientUtil;
import com.hotel.hoteltop.util.ResponseUtils;
import com.hotel.hoteltop.util.RpcResultDTO;
import com.taobao.api.request.XhotelRatesIncrementRequest;
import com.taobao.api.request.XhotelRatesUpdateRequest;
import com.taobao.api.response.XhotelRatesIncrementResponse;
import com.taobao.api.response.XhotelRatesUpdateResponse;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static com.hotel.hoteltop.util.ClientUtil.sessionKey;

@Service
public class PriceRuleService {


    @Resource
    private PriceRuleMapper priceRuleMapper;
    @Resource
    private PriceRuleDetailMapper priceRuleDetailMapper;

    public RpcResultDTO savePriceRuleInfo(PriceRuleModel model) {
        List<PriceRuleDetailModel> priceRuleDetailInfoList = model.getPriceRuleDetailInfoList();
        priceRuleMapper.insert(model);

        priceRuleDetailInfoList.forEach(e -> {
            e.setPriceId(Integer.valueOf(model.getId()));
            e.setCreateTime(new Date());
            priceRuleDetailMapper.insert(e);
        });

        return ResponseUtils.success();
    }

    public RpcResultDTO deletedPriceRuleInfo(Map<String, String> model) {
        String priceId = model.get("priceId");

        priceRuleMapper.deleteById(priceId);
        priceRuleDetailMapper.delete(new QueryWrapper<PriceRuleDetailModel>().eq("price_id", priceId));

        return ResponseUtils.success();
    }

    public RpcResultDTO updatePriceRuleInfo(PriceRuleModel model) {
        // 因为不知道传参形式，所有采用先删后插方式
        priceRuleMapper.deleteById(model.getId());
        priceRuleDetailMapper.delete(new QueryWrapper<PriceRuleDetailModel>().eq("price_id", model.getId()));
        model.setId(null);
        return savePriceRuleInfo(model);
    }
}
