package com.hotel.hoteltop.qdprice.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.hotel.hoteltop.constant.SysParamConstant;
import com.hotel.hoteltop.qdRateplan.mapper.RateplanMapper;
import com.hotel.hoteltop.qdRateplan.mapper.RateplanRecordMapper;
import com.hotel.hoteltop.qdRateplan.model.RateplanModel;
import com.hotel.hoteltop.qdRateplan.model.RateplanRecordModel;
import com.hotel.hoteltop.qdRateplan.service.RateplanService;
import com.hotel.hoteltop.qdprice.mapper.PriceMapper;
import com.hotel.hoteltop.util.*;
import com.taobao.api.request.XhotelRatesIncrementRequest;
import com.taobao.api.request.XhotelRatesUpdateRequest;
import com.taobao.api.response.XhotelRatesIncrementResponse;
import com.taobao.api.response.XhotelRatesUpdateResponse;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static com.hotel.hoteltop.util.ClientUtil.sessionKey;

@Service
public class PriceService {

    @Resource
    private PriceMapper priceMapper;
    @Resource
    private RateplanMapper rateplanMapper;
    @Resource
    private RateplanRecordMapper rateplanRecordMapper;
    @Resource
    private RateplanService rateplanService;


    public void savePriceCompleteInfo(HashMap<String, String> map) {
        ArrayList<Map<String, String>> priceList = new ArrayList<>();

        String hotelID = map.get("HotelID");
        ArrayList<RateplanModel> models = rateplanService.queryMaxTargetDateGroupByRoomTypeAndPlanRateCode(hotelID);

        models.forEach(e -> {
            Map<String, String> priceMap = new HashMap<>();
            priceMap.put("RoomTypeCode", e.getRoomTypeCode());
            priceMap.put("HotelNum", e.getHotelNum());
            priceMap.put("PlanRateCode", e.getPlanRateCode());

            priceList.add(priceMap);
        });
        computePriceToTop(priceList, true);
    }

    /**
     * 属于io密集型 需要多设置最大线程数量。
     */
    private final ExecutorService redisServiceExecutor =
            new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors() * 2, 1024, 30L, TimeUnit.SECONDS, new SynchronousQueue<>());

    @SneakyThrows
    public RpcResultDTO computePriceToTop(List<Map<String, String>> modelList, boolean isUpdate) {
        List<Map<String, Object>> toTopMap = new CopyOnWriteArrayList<>();

        List<Map<String, String>> failToTop = new CopyOnWriteArrayList<>();

        CountDownLatch countDownLatch = new CountDownLatch(modelList.size());
        for (Map<String, String> e : modelList) {
            redisServiceExecutor.execute(() -> {
                try {
                    String roomTypeCode = e.get("RoomTypeCode");
                    String hotelID = e.get("HotelNum");
                    String planRateCode = e.get("PlanRateCode");
                    // 判断产品信息记录表是否存在
                    RateplanRecordModel rateplanRecordModel = rateplanRecordMapper.selectOne(new QueryWrapper<RateplanRecordModel>().eq("hotel_id", hotelID)
                            .eq("plan_rate_code", planRateCode).eq("room_type_code", roomTypeCode));
                    if (rateplanRecordModel == null) {
                        failToTop.add(e);
                        return;
                    }

                    List<RateplanModel> rateplanModels = rateplanMapper.selectList(new QueryWrapper<RateplanModel>().eq("RoomTypeCode", roomTypeCode)
                            .eq("HotelNum", hotelID).eq("PlanRateCode", planRateCode).ge("TargetDate", DateUtil.getNowTime()));
                    Assert.notNull(rateplanModels, "未查询到价格数据");

                    HashMap<String, Object> map = new HashMap<>();
                    map.put("out_rid", SysParamConstant.HZ + hotelID + SysParamConstant.UNDERLINE + roomTypeCode);
                    map.put("rateplan_code", SysParamConstant.HZ + hotelID + SysParamConstant.UNDERLINE + roomTypeCode + SysParamConstant.UNDERLINE + planRateCode);

                    List<Map<String, Object>> inventoryPrice = new ArrayList<>();
                    // 查询每个产品对应的价格计算规则
                    rateplanModels.forEach(model -> {
                        String targetDate = model.getTargetDate();
                        Integer price = model.getPrice();

                        Map<String, String> pricesRule = priceMapper.queryPricesRuleForOne(hotelID, targetDate, price, planRateCode);
                        if (CollectionUtils.isEmpty(pricesRule)) {
                            pricesRule = priceMapper.queryPricesRuleForTwo(hotelID, targetDate, price, planRateCode);
                        }
                        if (CollectionUtils.isEmpty(pricesRule)) {
                            pricesRule = priceMapper.queryPricesRuleForThree(hotelID, targetDate, price, planRateCode);
                        }
                        if (CollectionUtils.isEmpty(pricesRule)) {
                            pricesRule = priceMapper.queryPricesRuleForFour(hotelID, targetDate, price, planRateCode);
                        }
                        // 组装对象 一组 HotelNum RoomTypeCode PlanRateCode
                        Map<String, Object> priceRule = constructToTop(pricesRule, model);
                        inventoryPrice.add(priceRule);
                    });
                    HashMap<String, Object> data = new HashMap<>();
                    data.put("use_room_inventory", false);
                    data.put("inventory_price", inventoryPrice);
                    map.put("data", data);
                    toTopMap.add(map);
                } catch (Exception exc) {
                    exc.printStackTrace();
                } finally {
                    countDownLatch.countDown();
                }

            });
        }
        try {
            countDownLatch.await();
        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
        }
        List<List<Map<String, Object>>> partList = TransferUtilFunction.partList(toTopMap, 29);

        for (List<Map<String, Object>> e : partList) {
            String jsonString = JSONObject.toJSONString(e);
            if (isUpdate) {
                XhotelRatesUpdateRequest req = new XhotelRatesUpdateRequest();
                req.setRateInventoryPriceMap(jsonString);
                XhotelRatesUpdateResponse rsp = ClientUtil.client.execute(req, sessionKey);
                System.out.println(rsp.getBody());
                if (StringUtils.isNotBlank(rsp.getErrorCode())) {
                    continue;
                }
            } else {
                XhotelRatesIncrementRequest req = new XhotelRatesIncrementRequest();
                req.setRateInventoryPriceMap(jsonString);
                XhotelRatesIncrementResponse rsp = ClientUtil.client.execute(req, sessionKey);
                System.out.println(rsp.getBody());
                if (StringUtils.isNotBlank(rsp.getErrorCode())) {
                    continue;
                }
            }
        }

        if (failToTop.size() > 0) {
            return ResponseUtils.error("推送部分数据失败 请检查以下数据是否存在对应产品信息=>" + JSONObject.toJSONString(failToTop));
        }

        return ResponseUtils.success("推送全量数据成功");
    }

    private Map<String, Object> constructToTop(Map<String, String> pricesRule, RateplanModel model) {
        HashMap<String, Object> priceMap = new HashMap<>();
        priceMap.put("date", model.getTargetDate());
        priceMap.put("quota", Integer.valueOf(model.getRoomCount()));
        priceMap.put("status", Integer.valueOf(model.getRoomCount()) > 0 ? 1 : 0);
        if (CollectionUtils.isEmpty(pricesRule)) {
            priceMap.put("price", (model.getPrice() + 5) * 100);
        } else {
            Object percentStr = pricesRule.get("percent");
            BigDecimal percent = new BigDecimal(percentStr.toString());
            Object addPriceStr = pricesRule.get("add_price");
            BigDecimal addPrice = new BigDecimal(addPriceStr.toString());
            BigDecimal price = new BigDecimal(model.getPrice());
            priceMap.put("price", (percent.multiply(price).add(addPrice)).multiply(new BigDecimal(100)));
        }
        return priceMap;
    }

    public RpcResultDTO incrementPriceToTop(List<Map<String, String>> modelList) {
        List<Map<String, String>> toTopList = new ArrayList<>(modelList.size());

        List<Map<String, String>> needToTopList = modelList.stream().filter(e -> {
            Boolean priceChange = Boolean.valueOf(e.get("PriceChange"));
            Boolean leftRoomNumChange = Boolean.valueOf(e.get("LeftRoomNumChange"));
            boolean hotelIdflag = StringUtils.isNotBlank(e.get("HotelID")) ? true : false;
            boolean rmTypeIDflag = StringUtils.isNotBlank(e.get("RmTypeID")) ? true : false;
            boolean planRateCodeflag = StringUtils.isNotBlank(e.get("PlanRateCode")) ? true : false;
            // 过滤酒店id 房型id 产品id 为空的数据
            if ((priceChange || leftRoomNumChange) && hotelIdflag && rmTypeIDflag && planRateCodeflag) {
                HashMap<String, String> map = new HashMap<>();
                map.put("RoomTypeCode", e.get("RmTypeID"));
                map.put("HotelNum", e.get("HotelID"));
                map.put("PlanRateCode", e.get("PlanRateCode"));

                toTopList.add(map);
                return true;
            }
            return false;
        }).collect(Collectors.toList());
        Assert.notNull(needToTopList, "未查询到产品对应的 价格以及数量变化");

        RpcResultDTO rpcResultDTO = computePriceToTop(toTopList, false);
        return ResponseUtils.success("推送变量数据 <" + toTopList.size() + ">条," + rpcResultDTO.getErrorMessage());
    }


}
