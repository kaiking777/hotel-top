package com.hotel.hoteltop.qdprice.controller;

import com.hotel.hoteltop.qdprice.service.PriceService;
import com.hotel.hoteltop.qdroom.service.RoomService;
import com.hotel.hoteltop.util.RpcResultDTO;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/priceManager")
public class PriceController {

    @Resource
    private PriceService priceService;


    /**
     * 全量
     * @param modelList
     * @return
     */
    @PostMapping(value = "/computePriceToTop")
    RpcResultDTO computePriceToTop(@RequestBody List<Map<String, String>> modelList) {
        Assert.notNull(modelList, "未获取到参数信息");
        return priceService.computePriceToTop(modelList, true);
    }

    /**
     * 增量
     * @param modelList
     * @return
     */
    @PostMapping(value = "/incrementPriceToTop")
    RpcResultDTO incrementPriceToTop(@RequestBody List<Map<String, String>> modelList) {
        Assert.notNull(modelList, "未获取到参数信息");
        return priceService.incrementPriceToTop(modelList);
    }

}
