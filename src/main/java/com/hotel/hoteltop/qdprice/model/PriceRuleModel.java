package com.hotel.hoteltop.qdprice.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Data
@TableName("price_rule_info")
public class PriceRuleModel implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    @TableField("hotel_id")
    private String hotelId;

    @TableField("group_id")
    private String groupId;

    @TableField("end_day")
    private String endDay;

    @TableField("start_day")
    private String startDay;

    @TableField("time_type")
    private Integer timeType;

    @TableField("create_time")
    private Date createTime;

    @TableField("create_username")
    private String createUsername;

    @TableField("upadte_username")
    private String upadteUsername;

    @TableField("update_time")
    private Date updateTime;

    @TableField(exist = false)
    private List<PriceRuleDetailModel> priceRuleDetailInfoList;

}