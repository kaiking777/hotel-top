package com.hotel.hoteltop.qdprice.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@TableName("price_rule_detail_info")
public class PriceRuleDetailModel implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    @TableField("price_id")
    private Integer priceId;

    @TableField("min_price")
    private Integer minPrice;

    @TableField("max_price")
    private Integer maxPrice;

    @TableField("percent")
    private BigDecimal percent;

    @TableField("add_price")
    private BigDecimal addPrice;

    @TableField("levels")
    private String levels;

    @TableField("create_time")
    private Date createTime;

    @TableField("create_username")
    private String createUsername;

    @TableField("update_username")
    private String updateUsername;

    @TableField("update_time")
    private Date updateTime;
}