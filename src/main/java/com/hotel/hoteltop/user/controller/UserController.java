package com.hotel.hoteltop.user.controller;

import com.hotel.hoteltop.qdhotel.model.HotelGroupDetailModel;
import com.hotel.hoteltop.qdhotel.model.HotelGroupModel;
import com.hotel.hoteltop.qdhotel.model.HotelGroupQueryModel;
import com.hotel.hoteltop.qdhotel.model.HotelQueryModel;
import com.hotel.hoteltop.qdhotel.service.HotelGroupDetailService;
import com.hotel.hoteltop.qdhotel.service.HotelGroupService;
import com.hotel.hoteltop.qdhotel.service.HotelService;
import com.hotel.hoteltop.qdprice.model.PriceRuleModel;
import com.hotel.hoteltop.qdprice.service.PriceRuleService;
import com.hotel.hoteltop.util.RpcResultDTO;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(value = "/userManager")
public class UserController {

    @Resource
    private HotelService hotelService;
    @Resource
    private HotelGroupService hotelGroupService;
    @Resource
    private HotelGroupDetailService hotelGroupDetailService;
    @Resource
    private PriceRuleService priceRuleService;


    /**
     * 酒店列表查询
     *
     * @param model
     * @return
     */
    @PostMapping(value = "/queryHotelInfoList")
    RpcResultDTO queryHotelInfoList(@RequestBody HotelQueryModel model) {
        return hotelService.queryHotelInfoList(model);
    }

    /**
     * 酒店组相关
     *
     * @param model
     * @return
     */
    @PostMapping(value = "/queryHotelGroupInfoList")
    RpcResultDTO queryHotelGroupInfoList(@RequestBody HotelGroupQueryModel model) {
        return hotelGroupService.queryHotelGroupInfoList(model);
    }

    @PostMapping(value = "/saveHotelGroupInfo")
    RpcResultDTO saveHotelGroupInfo(@RequestBody HotelGroupModel model) {
        return hotelGroupService.saveHotelGroupInfo(model);
    }

    @PostMapping(value = "/updateHotelGroupInfo")
    RpcResultDTO updateHotelGroupInfo(@RequestBody HotelGroupModel model) {
        return hotelGroupService.updateHotelGroupInfo(model);
    }

    @PostMapping(value = "/deletedHotelGroupInfo")
    RpcResultDTO deletedHotelGroupInfo(@RequestBody List<String> idList) {
        return hotelGroupService.deletedHotelGroupInfo(idList);
    }

    /**
     * 酒店组明细相关
     * @param model
     * @return
     */
    @PostMapping(value = "/queryHotelGroupDetailInfoList")
    RpcResultDTO queryHotelGroupDetailInfoList(@RequestBody HotelGroupQueryModel model) {
        return hotelGroupDetailService.queryHotelGroupDetailInfoList(model);
    }

    @PostMapping(value = "/saveHotelDetailGroupInfo")
    RpcResultDTO saveHotelDetailGroupInfo(@RequestBody Map<String, Object> model) {
        Assert.notNull(model, "未获取到参数信息");
        return hotelGroupDetailService.saveHotelDetailGroupInfo(model);
    }

    @PostMapping(value = "/deletedHotelDetailGroupInfo")
    RpcResultDTO deletedHotelDetailGroupInfo(@RequestBody Map<String, Object> model) {
        Assert.notNull(model, "未获取到参数信息");
        return hotelGroupDetailService.deletedHotelDetailGroupInfo(model);
    }

    /**
     * 价格规则相关设置
     */
    @PostMapping(value = "/savePriceRuleInfo")
    RpcResultDTO savePriceRuleInfo(@RequestBody PriceRuleModel model) {
        Assert.notNull(model, "未获取到参数信息");
        return priceRuleService.savePriceRuleInfo(model);
    }

    @PostMapping(value = "/deletedPriceRuleInfo")
    RpcResultDTO deletedPriceRuleInfo(@RequestBody Map<String, String> model) {
        Assert.notNull(model, "未获取到参数信息");
        return priceRuleService.deletedPriceRuleInfo(model);
    }

    @PostMapping(value = "/updatePriceRuleInfo")
    RpcResultDTO updatePriceRuleInfo(@RequestBody PriceRuleModel model) {
        Assert.notNull(model, "未获取到参数信息");
        return priceRuleService.updatePriceRuleInfo(model);
    }

    /**
     * 上架。下架.
     */
    @PostMapping(value = "/updateHotelStatus")
    RpcResultDTO updateHotelStatus(@RequestBody Map<String, Object> model) {
        return hotelService.updateHotelStatus(model);
    }
}
