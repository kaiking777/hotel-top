package com.hotel.hoteltop.qdhotel.controller;

import com.hotel.hoteltop.constant.HotelStatusEnum;
import com.hotel.hoteltop.qdhotel.service.HotelService;
import com.hotel.hoteltop.util.RpcResultDTO;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(value = "/hotelManager")
public class HotelController {

    @Resource
    private HotelService hotelService;

    @PostMapping(value = "/saveBatchHotelCompleteInfo")
    RpcResultDTO saveBatchHotelCompleteInfo(@RequestBody List<String> idList) {
        Assert.notNull(idList, "未获取到酒店id");
        return hotelService.saveBatchHotelCompleteInfo(idList);
    }

    @PostMapping(value = "/saveBatchHotel")
    RpcResultDTO saveBatchHotel(@RequestBody List<String> idList) {
        Assert.notNull(idList, "未获取到酒店id");
        return hotelService.saveHotel(idList);
    }

    @GetMapping(value = "/queryHotel")
    RpcResultDTO queryHotel(String hotelId) {
        Assert.notNull(hotelId, "未获取到酒店id");
        return hotelService.queryHotel(hotelId);
    }

    /**
     * 更新酒店
     * @param map
     * @return
     */
    @PostMapping(value = "/modifyBatchHotel")
    RpcResultDTO modifyHotel(@RequestBody Map<String, Object> map) {
        Assert.notNull(map.get("idList"), "未获取到酒店id");
        return hotelService.modifyHotel((List<String>) map.get("idList"), String.valueOf(map.get("reason")), HotelStatusEnum.normal.getCode());
    }

    @PostMapping(value = "/delHotel")
    RpcResultDTO delHotel(@RequestBody List<String> idList) {
        Assert.notNull(idList, "未获取到酒店id");
        return hotelService.delHotel(idList);
    }
}
