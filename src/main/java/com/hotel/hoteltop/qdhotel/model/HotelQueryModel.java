package com.hotel.hoteltop.qdhotel.model;


import com.hotel.hoteltop.util.PageHelper;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class HotelQueryModel extends PageHelper implements Serializable {

    private List<String> hotelIds;

    private String cityName;

    private String hotelName;

    private String hid;

    // 酒店状态：0: 正常;-2:停售；-1：删除
    private String status;

}