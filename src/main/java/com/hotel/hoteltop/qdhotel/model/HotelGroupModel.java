package com.hotel.hoteltop.qdhotel.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("hotel_group_info")
public class HotelGroupModel implements Serializable {

    @TableId(value = "group_id", type = IdType.AUTO)
    private String groupId;

    @TableField("create_time")
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;

    @TableField("group_name")
    private String groupName;

    @TableField("`desc`")
    private String desc;

    @TableField("create_username")
    private String createUsername;

    @TableField("update_username")
    private String updateUsername;
}