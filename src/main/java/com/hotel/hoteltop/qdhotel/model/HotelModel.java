package com.hotel.hoteltop.qdhotel.model;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("HOTEL_INFOS")
public class HotelModel implements Serializable {

    @TableField("id")
    private String id;

    @TableField("created_at")
    private Date createdAt;

    @TableField("updated_at")
    private Date updatedAt;

    @TableField("deleted_at")
    private Date deletedAt;

    @TableField("ExpediaHotelId")
    private String expediaHotelId;

    @TableField("HotelsId")
    private String hotelsId;

    @TableField("Latitude")
    private String latitude;

    @TableField("Longitude")
    private String longitude;

    @TableField("HotelName_Zh")
    private String hotelNameZh;

    @TableField("HotelName_En")
    private String hotelNameEn;

    @TableField("City_Zh")
    private String cityZh;

    @TableField("City_En")
    private String cityEn;

    @TableField("City_Code")
    private Long cityCode;

    @TableField("Province_En")
    private String provinceEn;

    @TableField("Province_Zh")
    private String provinceZh;

    @TableField("Province_Code")
    private Long provinceCode;

    @TableField("Street_Zh")
    private String streetZh;

    @TableField("Street_En")
    private String streetEn;

    @TableField("Address")
    private String address;

    @TableField("Address_En")
    private String addressEn;

    @TableField("Country")
    private String country;

    @TableField("Country_En")
    private String countryEn;

    @TableField("HotelsStar")
    private String hotelsStar;

    @TableField("ExpediaUrlName")
    private String expediaUrlName;

    @TableField("Examine")
    private String examine;

    @TableField("Original_HotelName_Zh")
    private String originalHotelNameZh;

    @TableField("Original_HotelName_En")
    private String originalHotelNameEn;

    @TableField("Original_Address")
    private String originalAddress;

    @TableField("HotelTel")
    private String hotelTel;

    @TableField("other")
    private String other;
}