package com.hotel.hoteltop.qdhotel.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
/**
 * 飞猪端酒店信息记录表
 */
@Data
@TableName("HOTEL_INFOS_RECORD")
public class HotelRecordModel implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    @TableField("hid")
    private Long hid;

    @TableField("status")
    private Integer status;

    @TableField("s_hotel_shid")
    private Integer sHotelShid;

    @TableField("s_hotel_name")
    private String sHotelName;

    @TableField("s_hotel_address")
    private String sHotelAddress;

    @TableField("s_hotel_tel")
    private String sHotelTel;

    @TableField("hotel_id")
    private String hotelId;

    @TableField("reason")
    private String reason;
}