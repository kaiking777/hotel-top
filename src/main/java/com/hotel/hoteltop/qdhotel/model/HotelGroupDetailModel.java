package com.hotel.hoteltop.qdhotel.model;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@TableName("hotel_group_detail_info")
public class HotelGroupDetailModel implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    @TableField("group_id")
    private String groupId;

    @TableField("create_time")
    private Date createTime;

    @TableField("hotel_id")
    private String hotelId;

    @TableField("create_username")
    private String createUsername;
}