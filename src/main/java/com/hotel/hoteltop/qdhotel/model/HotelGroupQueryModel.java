package com.hotel.hoteltop.qdhotel.model;


import com.hotel.hoteltop.util.PageHelper;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class HotelGroupQueryModel extends PageHelper implements Serializable {

    private String groupName;
    private String groupId;

}