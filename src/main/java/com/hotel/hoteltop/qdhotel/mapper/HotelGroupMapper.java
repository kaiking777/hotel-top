package com.hotel.hoteltop.qdhotel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.hoteltop.qdhotel.model.HotelGroupModel;
import com.hotel.hoteltop.qdhotel.model.HotelRecordModel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface HotelGroupMapper extends BaseMapper<HotelGroupModel> {
}
