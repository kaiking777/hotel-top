package com.hotel.hoteltop.qdhotel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.hoteltop.qdhotel.model.HotelModel;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@Mapper
public interface HotelMapper extends BaseMapper<HotelModel> {


    @Select("<script>" +
            "SELECT i.*, r.* from hotel_infos i " +
            "   LEFT JOIN hotel_infos_record r on i.HotelsId = r.hotel_id " +
            "   where 1 = 1 " +
            "   <if test='cityName != null'> and i.City_Zh like CONCAT('%', #{cityName}, '%') </if> " +
            "   <if test='hotelName != null'> and i.HotelName_Zh like CONCAT('%', #{hotelName}, '%') </if> " +
            "   <if test='hid != null'> and r.hid = #{hid} </if> " +
            "   <if test='status != null'> and r.status = #{status} </if> " +
            "   <if test='hotelIds != null'> and i.HotelsId in " +
            "       <foreach item='item' index='index' collection='hotelIds' open='(' separator=',' close=')'>#{item}</foreach>" +
            "   </if> " +
            "</script>")
    List<Map<String, Object>> queryHotelInfoList(@Param("hotelIds") List<String> hotelIds, @Param("cityName") String cityName,
                                                 @Param("hotelName") String hotelName, @Param("hid") String hid, @Param("status") String status);

}
