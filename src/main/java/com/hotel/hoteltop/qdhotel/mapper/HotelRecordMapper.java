package com.hotel.hoteltop.qdhotel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hotel.hoteltop.qdhotel.model.HotelModel;
import com.hotel.hoteltop.qdhotel.model.HotelRecordModel;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface HotelRecordMapper extends BaseMapper<HotelRecordModel> {
}
