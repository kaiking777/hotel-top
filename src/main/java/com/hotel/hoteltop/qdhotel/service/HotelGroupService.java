package com.hotel.hoteltop.qdhotel.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hotel.hoteltop.constant.SysParamConstant;
import com.hotel.hoteltop.qdRateplan.service.RateplanService;
import com.hotel.hoteltop.qdhotel.mapper.HotelGroupDetailMapper;
import com.hotel.hoteltop.qdhotel.mapper.HotelGroupMapper;
import com.hotel.hoteltop.qdhotel.mapper.HotelMapper;
import com.hotel.hoteltop.qdhotel.mapper.HotelRecordMapper;
import com.hotel.hoteltop.qdhotel.model.*;
import com.hotel.hoteltop.qdprice.mapper.PriceRuleDetailMapper;
import com.hotel.hoteltop.qdprice.mapper.PriceRuleMapper;
import com.hotel.hoteltop.qdprice.model.PriceRuleDetailModel;
import com.hotel.hoteltop.qdprice.model.PriceRuleModel;
import com.hotel.hoteltop.qdprice.service.PriceService;
import com.hotel.hoteltop.qdroom.service.RoomService;
import com.hotel.hoteltop.util.ClientUtil;
import com.hotel.hoteltop.util.ResponseUtils;
import com.hotel.hoteltop.util.RpcResultDTO;
import com.hotel.hoteltop.util.TransferUtilFunction;
import com.taobao.api.request.XhotelAddRequest;
import com.taobao.api.request.XhotelDeleteRequest;
import com.taobao.api.request.XhotelGetRequest;
import com.taobao.api.request.XhotelUpdateRequest;
import com.taobao.api.response.XhotelAddResponse;
import com.taobao.api.response.XhotelGetResponse;
import com.taobao.api.response.XhotelUpdateResponse;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hotel.hoteltop.util.ClientUtil.sessionKey;

@Service
public class HotelGroupService {

    @Resource
    private HotelGroupMapper hotelGroupMapper;
    @Resource
    private HotelGroupDetailMapper hotelGroupDetailMapper;
    @Resource
    private PriceRuleMapper priceRuleMapper;
    @Resource
    private PriceRuleDetailMapper priceRuleDetailMapper;


    public RpcResultDTO<PageInfo<Map<String, Object>>> queryHotelGroupInfoList(HotelGroupQueryModel model) {
        Page<Map<String, Object>> page = PageHelper.startPage(model.getPageNo(), model.getPageSize());
        QueryWrapper<HotelGroupModel> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(model.getGroupName())) {
            wrapper.like("group_name", model.getGroupName());
        }
        List<HotelGroupModel> hotelGroupModels = hotelGroupMapper.selectList(wrapper);
        // 此处需要拼接价格规则数据 通过groupid查询 组装出来一个 list<map>
        List<Map<String, Object>> dataList = new ArrayList<>();
        hotelGroupModels.forEach(e -> {
            HashMap<String, Object> hashMap = new HashMap<>();
            String groupId = e.getGroupId();
            if (StringUtils.isBlank(groupId)) {
                return;
            }
            List<PriceRuleModel> priceRuleModelList = priceRuleMapper.selectList(new QueryWrapper<PriceRuleModel>().eq("group_id", groupId));

            priceRuleModelList.forEach(priceRuleModel -> {
                List<PriceRuleDetailModel> ruleDetailModelList = priceRuleDetailMapper.selectList(new QueryWrapper<PriceRuleDetailModel>().eq("price_id", priceRuleModel.getId()));
                priceRuleModel.setPriceRuleDetailInfoList(ruleDetailModelList);
            });
            hashMap.put("group", e);
            hashMap.put("priceRuleInfo", priceRuleModelList);
            dataList.add(hashMap);
        });

        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(dataList);
        pageInfo.setTotal(page.getTotal());
        return ResponseUtils.success(pageInfo);
    }

    public RpcResultDTO saveHotelGroupInfo(HotelGroupModel model) {
        hotelGroupMapper.insert(model);
        return ResponseUtils.success("新增成功");
    }

    public RpcResultDTO updateHotelGroupInfo(HotelGroupModel model) {
        hotelGroupMapper.updateById(model);
        return ResponseUtils.success("修改成功");
    }

    /**
     * 需要删除组内数据
     *
     * @param model
     * @return
     */
    public RpcResultDTO deletedHotelGroupInfo(List<String> model) {
        hotelGroupMapper.deleteBatchIds(model);
        hotelGroupDetailMapper.delete(new QueryWrapper<HotelGroupDetailModel>().in("group_id", model));
        return ResponseUtils.success("级联删除成功");
    }
}
