package com.hotel.hoteltop.qdhotel.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hotel.hoteltop.constant.HotelStatusEnum;
import com.hotel.hoteltop.constant.SysParamConstant;
import com.hotel.hoteltop.qdRateplan.service.RateplanService;
import com.hotel.hoteltop.qdhotel.mapper.HotelGroupDetailMapper;
import com.hotel.hoteltop.qdhotel.mapper.HotelGroupMapper;
import com.hotel.hoteltop.qdhotel.mapper.HotelMapper;
import com.hotel.hoteltop.qdhotel.mapper.HotelRecordMapper;
import com.hotel.hoteltop.qdhotel.model.*;
import com.hotel.hoteltop.qdprice.mapper.PriceRuleDetailMapper;
import com.hotel.hoteltop.qdprice.mapper.PriceRuleMapper;
import com.hotel.hoteltop.qdprice.model.PriceRuleDetailModel;
import com.hotel.hoteltop.qdprice.model.PriceRuleModel;
import com.hotel.hoteltop.qdprice.service.PriceService;
import com.hotel.hoteltop.qdroom.service.RoomService;
import com.hotel.hoteltop.util.*;
import com.taobao.api.request.*;
import com.taobao.api.response.XhotelAddResponse;
import com.taobao.api.response.XhotelGetResponse;
import com.taobao.api.response.XhotelUpdateResponse;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.hotel.hoteltop.util.ClientUtil.sessionKey;

@Service
public class HotelService {

    @Resource
    private HotelMapper hotelMapper;
    @Resource
    private HotelRecordMapper hotelRecordMapper;
    @Resource
    private RoomService roomService;
    @Resource
    private RateplanService rateplanService;
    @Resource
    private PriceService priceService;
    @Resource
    private PriceRuleMapper priceRuleMapper;
    @Resource
    private PriceRuleDetailMapper priceRuleDetailMapper;


    public RpcResultDTO saveBatchHotelCompleteInfo(List<String> idList) {
        idList.forEach(e -> {
            ArrayList<String> list = new ArrayList<>();
            list.add(e);
            HashMap<String, String> map = new HashMap<>();
            map.put("HotelID", e);
            // 酒店
            saveHotel(list);
            // 房型
            roomService.saveRoomCompleteInfo(map);
            // 产品
            rateplanService.saveRoomCompleteInfo(map);
            // 价格
            priceService.savePriceCompleteInfo(map);
            // 新增房型时接口没有status状态的传输，所以新增房型是不售卖的状态，最后需要把房型状态改为正常
            roomService.modifyBatchRoom(map, HotelStatusEnum.normal.getCode());

            System.out.println("一套的流程：酒店、房型、产品、价格 新增成功 HotelId-》" + e);
        });
        return ResponseUtils.success();
    }


    @SneakyThrows
    public RpcResultDTO<String> saveHotel(List<String> idList) {
        List<HotelModel> selectList = hotelMapper.selectList(new QueryWrapper<HotelModel>().in("HotelsId", idList));
        ArrayList<String> failHotelList = new ArrayList<>();

        for (HotelModel e : selectList) {
            XhotelAddRequest request = TransferUtilFunction.transferHotelModelAddFunc.apply(e);
            XhotelAddResponse rsp = ClientUtil.client.execute(request, sessionKey);
            if (StringUtils.isNotBlank(rsp.getErrorCode())) {
                System.out.println(rsp.getMsg());
                failHotelList.add(e.getHotelsId());
                continue;
            }
            toHotelRecord(rsp.getBody(), e.getHotelsId(), "add", SysParamConstant.empty);
        }
        if (failHotelList.size() > 0) {
            return ResponseUtils.error("推送酒店信息部分失败 =>" + failHotelList.toString());
        }

        return ResponseUtils.success("推送酒店信息成功，并成功落库hotel_infos_record");
    }

    private void toHotelRecord(String bodyInfo, String hotelId, String operateStr, String reason) {
        Map parseObject = JSONObject.parseObject(bodyInfo, Map.class);
        Map response = (Map) parseObject.get("xhotel_" + operateStr + "_response");
        Map xhotel = (Map) response.get("xhotel");

        HotelRecordModel recordModel = new HotelRecordModel();
        recordModel.setHid((Long) xhotel.get("hid"));
        recordModel.setStatus(Integer.valueOf(xhotel.get("status").toString()));

        Map<String, Object> sHotelMap = (Map) xhotel.get("s_hotel");
        recordModel.setSHotelAddress(sHotelMap.get("address").toString());
        recordModel.setSHotelName(sHotelMap.get("name").toString());
        recordModel.setSHotelShid(Integer.valueOf(sHotelMap.get("shid").toString()));
        recordModel.setSHotelTel(sHotelMap.get("tel").toString());
        recordModel.setReason(reason);

        recordModel.setHotelId(hotelId);

        // 此处应该要保证唯一的hotel_id
        hotelRecordMapper.delete(new QueryWrapper<HotelRecordModel>().eq("hotel_id", hotelId));
        hotelRecordMapper.insert(recordModel);
    }

    @SneakyThrows
    public RpcResultDTO queryHotel(String hotelId) {
        XhotelGetRequest req = new XhotelGetRequest();
        req.setOuterId(SysParamConstant.HZ + hotelId);
        req.setNeedSaleInfo(true);
        XhotelGetResponse rsp = ClientUtil.client.execute(req, sessionKey);
        if (rsp.isSuccess()) {
            return ResponseUtils.success(rsp.getBody());
        }
        return ResponseUtils.error(rsp.getSubMsg());
    }


    @SneakyThrows
    public RpcResultDTO modifyHotel(List<String> idList, String reason, String hotelStatus) {
        List<HotelModel> selectList = hotelMapper.selectList(new QueryWrapper<HotelModel>().in("HotelsId", idList));
        for (HotelModel e : selectList) {
            HotelRecordModel recordModel = hotelRecordMapper.selectOne(new QueryWrapper<HotelRecordModel>().eq("hotel_id", e.getHotelsId()));
            if (recordModel == null) {
                continue;
            }
            XhotelUpdateRequest request = TransferUtilFunction.transferHotelModelModifyFunc.apply(e);
            request.setStatus(Byte.valueOf(hotelStatus));
            XhotelUpdateResponse rsp = ClientUtil.client.execute(request, sessionKey);
            if (StringUtils.isNotBlank(rsp.getErrorCode())) {
                System.out.println(rsp.getMsg());
                continue;
            }
            // 上面接口淘宝解释为不存在则自动新增，所以有必要在进行入库
            toHotelRecord(rsp.getBody(), e.getHotelsId(), "update", reason);
        }

        return ResponseUtils.success("推送修改信息成功");
    }

    /**
     * 批量删除酒店 确定要把酒店信息 房型信息 产品信息 全删除
     * 是只删除飞猪端信息记录表
     *
     * @param idList
     * @return
     */
    @SneakyThrows
    public RpcResultDTO delHotel(List<String> idList) {
        for (String e : idList) {
            HotelRecordModel recordModel = hotelRecordMapper.selectOne(new QueryWrapper<HotelRecordModel>().eq("hotel_id", e));
            if (recordModel == null) {
                return ResponseUtils.error("未查询到酒店信息==>" + e);
            }
            XhotelDeleteRequest hotelReq = new XhotelDeleteRequest();
            hotelReq.setOuterId(SysParamConstant.HZ + e);
            hotelReq.setHid(recordModel.getHid());
            ClientUtil.client.execute(hotelReq, sessionKey);
        }
        // 删除飞猪端酒店信息记录表
        hotelRecordMapper.delete(new QueryWrapper<HotelRecordModel>().in("hotel_id", idList));

        roomService.delBatchRoom(idList);

        rateplanService.delBatchRateplanForHotel(idList);

        return ResponseUtils.success("级联删除成功");
    }

    public RpcResultDTO<PageInfo<Map<String, Object>>> queryHotelInfoList(HotelQueryModel model) {
        Page<Map<String, Object>> page = PageHelper.startPage(model.getPageNo(), model.getPageSize());
        List<Map<String, Object>> infoList = hotelMapper.queryHotelInfoList(model.getHotelIds(), model.getCityName(), model.getHotelName(), model.getHid(), model.getStatus());
        // 此处需要拼接价格规则数据 通过hoteid查询
        List<Map<String, Object>> dataList = new ArrayList<>();
        infoList.forEach(e -> {
            HashMap<String, Object> hashMap = new HashMap<>();
            String hotelsId = String.valueOf(e.get("HotelsId"));
            if (StringUtils.isBlank(hotelsId)) {
                return;
            }
            List<PriceRuleModel> priceRuleModelList = priceRuleMapper.selectList(new QueryWrapper<PriceRuleModel>().eq("hotel_id", hotelsId));

            priceRuleModelList.forEach(priceRuleModel -> {
                List<PriceRuleDetailModel> ruleDetailModelList = priceRuleDetailMapper.selectList(new QueryWrapper<PriceRuleDetailModel>().eq("price_id", priceRuleModel.getId()));
                priceRuleModel.setPriceRuleDetailInfoList(ruleDetailModelList);
            });
            hashMap.put("hotelInfo", e);
            hashMap.put("priceRuleInfo", priceRuleModelList);
            dataList.add(hashMap);
        });
        PageInfo<Map<String, Object>> pageInfo = new PageInfo<>(dataList);
        pageInfo.setTotal(page.getTotal());
        return ResponseUtils.success(pageInfo);
    }

    // 酒店状态：0: 正常;-2:停售；-1：删除
    public RpcResultDTO updateHotelStatus(Map<String, Object> model) {
        List idList = (List) model.get("idList");
        String status = String.valueOf(model.get("status"));
        String reason = String.valueOf(model.get("reason"));

        modifyHotel(idList, reason, status);

        return ResponseUtils.success();
    }
}
