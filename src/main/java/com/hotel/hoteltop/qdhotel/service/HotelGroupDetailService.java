package com.hotel.hoteltop.qdhotel.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.hotel.hoteltop.qdhotel.mapper.HotelGroupDetailMapper;
import com.hotel.hoteltop.qdhotel.mapper.HotelGroupMapper;
import com.hotel.hoteltop.qdhotel.mapper.HotelMapper;
import com.hotel.hoteltop.qdhotel.mapper.HotelRecordMapper;
import com.hotel.hoteltop.qdhotel.model.HotelGroupDetailModel;
import com.hotel.hoteltop.qdhotel.model.HotelGroupModel;
import com.hotel.hoteltop.qdhotel.model.HotelGroupQueryModel;
import com.hotel.hoteltop.qdhotel.model.HotelModel;
import com.hotel.hoteltop.util.DateUtil;
import com.hotel.hoteltop.util.ResponseUtils;
import com.hotel.hoteltop.util.RpcResultDTO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class HotelGroupDetailService {

    @Resource
    private HotelGroupDetailMapper hotelGroupDetailMapper;
    @Resource
    private HotelMapper hotelMapper;


    public RpcResultDTO saveHotelDetailGroupInfo(Map<String, Object> model) {
        String groupId = String.valueOf(model.get("groupId"));
        List<String> hotelIds = (List) model.get("hotelIds");
        ArrayList<String> failList = new ArrayList<>();
        hotelIds.forEach(e -> {
            // hotel_id是hotel_infos里面的
            HotelModel hotelModel = hotelMapper.selectOne(new QueryWrapper<HotelModel>().eq("HotelsId", e));
            if (hotelModel == null) {
                failList.add(e);
                return;
            }
            // 一个酒店只能在一个组，
            HotelGroupDetailModel groupDetailModel = hotelGroupDetailMapper.selectOne(new QueryWrapper<HotelGroupDetailModel>().eq("hotel_id", e));
            if (groupDetailModel != null) {
                failList.add(e);
                return;
            }
            HotelGroupDetailModel detailModel = new HotelGroupDetailModel();
            detailModel.setHotelId(e);
            detailModel.setCreateTime(new Date());
            detailModel.setGroupId(groupId);
            hotelGroupDetailMapper.insert(detailModel);
        });
        if (failList.size() > 0) {
            return ResponseUtils.error("存在重复书数据 => " + JSONObject.toJSONString(failList));
        }
        return ResponseUtils.success("成功");
    }

    public RpcResultDTO deletedHotelDetailGroupInfo(Map<String, Object> model) {
        String groupId = String.valueOf(model.get("groupId"));
        List<String> hotelIds = (List) model.get("hotelIds");

        hotelGroupDetailMapper.delete(new QueryWrapper<HotelGroupDetailModel>()
                .eq("group_id", groupId)
                .in("hotel_id", hotelIds));
        return ResponseUtils.success();
    }


    public RpcResultDTO queryHotelGroupDetailInfoList(HotelGroupQueryModel model) {
        Page<HotelGroupDetailModel> page = PageHelper.startPage(model.getPageNo(), model.getPageSize());

        List<HotelGroupDetailModel> groupDetailModelList = hotelGroupDetailMapper.selectList(new QueryWrapper<HotelGroupDetailModel>().eq("group_id", model.getGroupId()));
        PageInfo<HotelGroupDetailModel> pageInfo = new PageInfo<>(groupDetailModelList);
        pageInfo.setTotal(page.getTotal());
        return ResponseUtils.success(pageInfo);
    }
}
